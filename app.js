var express = require('express'),
    app       = express(),
    server    = require('http').createServer(app),
    io        = require('socket.io').listen(server),
    mongoose  = require('mongoose'),
    bodyParser = require('body-parser'),
    users     = {}
    ;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var port = process.env.PORT || 8080;
var mongoUrl = process.env.MONGOLAB_URI || 'mongodb://localhost/db';
server.listen(port);
console.log('Listening on port '+port+'!');

mongoose.connect( mongoUrl, function(err){
  if(err){
    console.log("MONGO ERROR!", err);
  } else{
    console.log('Connected to mongodb!');
  }
});

var chatSchema = mongoose.Schema({
  nick: String,
  msg: String,
  created: {type: Date, default: Date.now}
});

var dbRowSchema = mongoose.Schema({
  post_id: Number,
  post_title: String,
  post_content: String,
  created: {type: Date, default: Date.now}
});

var Chat = mongoose.model('Message', chatSchema);
var DbRows = mongoose.model('DbRow', dbRowSchema);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


/*=======================================
=            API: Request ID            =
=======================================*/
app.post('/checkin', function(req, res, next){
  console.log(req.body);

  var newMsg = new DbRows({
      post_id: req.body.post_id,
      post_title: req.body.post_title,
      post_content: req.body.post_content
    });

  newMsg.save(function(err){
    if(err) throw err;
    io.sockets.emit('new dbrow', {
      post_id: req.body.post_id,
      post_title: req.body.post_title,
      post_content: req.body.post_content
    });
  });

  io.sockets.emit('new checkin', req.body);

  res.send({
    message: "success"
  });

});


io.sockets.on('connection', function(socket){
  var query = DbRows.find({});
  query.sort('-created').limit(8).exec(function(err, docs){
    if(err) throw err;
    socket.emit('load dbrows', docs);
  });

});

/*====================================
=            SOCKET: Chat            =
====================================*/

io.sockets.on('connection', function(socket){
  var query = Chat.find({});
  query.sort('-created').limit(8).exec(function(err, docs){
    if(err) throw err;
    socket.emit('load old msgs', docs);
  });

  socket.on('new user', function(data, callback){
    if (data in users){
      callback(false);
    } else{
      callback(true);
      socket.nickname = data;
      users[socket.nickname] = socket;
      updateNicknames();
    }
  });

  function updateNicknames(){
    io.sockets.emit('usernames', Object.keys(users));
  }

  socket.on('send message', function(data, callback){
    var msg = data.trim();
    console.log('after trimming message is: ' + msg);
    if(msg.substr(0,3) === '/w '){
      msg = msg.substr(3);
      var ind = msg.indexOf(' ');
      if(ind !== -1){
        var name = msg.substring(0, ind);
        msg = msg.substring(ind + 1);
        if(name in users){
          users[name].emit('whisper', {msg: msg, nick: socket.nickname});
          console.log('message sent is: ' + msg);
          console.log('Whisper!');
        } else{
          callback('Error!  Enter a valid user.');
        }
      } else{
        callback('Error!  Please enter a message for your whisper.');
      }
    } else{
      var newMsg = new Chat({msg: msg, nick: socket.nickname});
      newMsg.save(function(err){
        if(err) throw err;
        io.sockets.emit('new message', {msg: msg, nick: socket.nickname});
      });
    }
  });

  socket.on('disconnect', function(data){
    if(!socket.nickname) return;
    delete users[socket.nickname];
    updateNicknames();
  });

});